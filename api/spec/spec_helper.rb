require_relative "routes/signup"
require_relative "routes/sessions"
require_relative "routes/equipos"

require_relative "libs/mongo"
require_relative "helpers"

require "digest/md5"

def to_md5(pass)
  return Digest::MD5.hexdigest(pass)
end

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

  config.before(:suite) do
    users = [
      { name: "Pablito", email: "pablito@mexico.com", password: to_md5("mexico") },
      { name: "Laranja", email: "la@ranja.com", password: to_md5("123") },
      { name: "Picolo", email: "pi@colo.com", password: to_md5("123") },
      { name: "Joe Silva", email: "joe@bol.com", password: to_md5("123") },
      { name: "Edward Santos", email: "ed@bol.com", password: to_md5("123") },
    ]

    MongoDB.new.drop_danger
    MongoDB.new.insert_users(users)
  end
end
