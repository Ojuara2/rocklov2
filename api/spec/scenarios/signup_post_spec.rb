describe "POST / signup" do
  context "Novo usuario" do
    before(:all) do
      payload = { name: "Mando", email: "mando@gmail.com", password: "pwd123" }
      MongoDB.new.remove_user(payload[:email])

      @result = Signup.new.create(payload)
    end

    it "Valida status code" do
      expect(@result.code).to eq 200
    end

    it "Valida id do usuário" do
      expect(@result.parsed_response["_id"].length).to eq 24
    end
  end

  context "usuario ja existe" do
    before(:all) do
      #dado que tenho um novo usuario
      payload = { name: "Silva", email: "silva@bol.com", password: "123" }
      MongoDB.new.remove_user(payload[:email])

      # e o email desse usuario ja foi cadastrado no sistema
      Signup.new.create(payload)

      #quando eu faco uma requisicao para a rota/signup
      @result = Signup.new.create(payload)
    end

    it "Deve retornar 409" do
      #entao deve retornar 409
      expect(@result.code).to eq 409
    end

    it "Deve retornar mensagem" do
      expect(@result.parsed_response["error"]).to eq "Email already exists :("
    end
  end
end
