#DRY Don`t Repeat Yourself = Não se repita
# DRY X TESTE

describe "POST /sessions" do
  context "login com sucesso" do
    before(:all) do
      payload = { email: "pablito@mexico.com", password: "mexico" }
      @result = Sessions.new.login(payload)
    end
    it "Valida status code" do
      expect(@result.code).to eq 200
    end

    it "Valida id do usuário" do
      expect(@result.parsed_response["_id"].length).to eq 24
    end
  end

  # examples = [
  #   {
  #     title: "Senha incorreta",
  #     payload: { email: "pablito@mexico.com", password: "123" },
  #     code: 401,
  #     error: "Unauthorized",
  #   },
  #   {
  #     title: "Usuario não existe",
  #     payload: { email: "404@mexico.com", password: "123" },
  #     code: 401,
  #     error: "Unauthorized",
  #   },
  #   {
  #     title: "Email em branco",
  #     payload: { email: "", password: "123" },
  #     code: 412,
  #     error: "required email",
  #   },
  #   {
  #     title: "Sem o campo email",
  #     payload: { password: "123" },
  #     code: 412,
  #     error: "required email",
  #   },
  #   {
  #     title: "Senha em branco",
  #     payload: { email: "pablito@mexico.com", password: "" },
  #     code: 412,
  #     error: "required password",
  #   },
  #   {
  #     title: "Sem o campo senha",
  #     payload: { email: "pablito@mexico.com" },
  #     code: 412,
  #     error: "required password",
  #   },

  # ]

  examples = Helpers::get_fixture("login")

  examples.each do |e|
    context "#{e[:title]}" do
      before(:all) do
        @result = Sessions.new.login(e[:payload])
      end
      it "Valida status code #{e[:code]}" do
        expect(@result.code).to eq e[:code]
      end

      it "Valida id do usuário" do
        expect(@result.parsed_response["error"]).to eq e[:error]
      end
    end
  end
end
